import React, { Component } from 'react';
import axios from 'axios';
import { debounce } from 'lodash';
import { api_key } from "./secretVariables";
import './styles/autocomplete.sass';
import './styles/common.sass';


class Autocomplete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userInput: this.props.userInput,
            filteredMovies: []
        };
        this.fetchDataFromApi = debounce(this.fetchDataFromApi, 100);
    }

    fetchDataFromApi = (userInput) => {
        const minUserInputLength = 3;
        const maxNumOfOutputs = 8;

        const apiUrl = `https://api.themoviedb.org/3/search/movie?api_key=${api_key}&language=en-US&query=`;

        // trigger search only if the length of input text is greater than or equal to the predetermined threshold
        if (userInput.length >= minUserInputLength) {
            axios.get(apiUrl + userInput).then(response => {
                // limit the number of search outputs
                const filteredMovies = response.data.results.slice(0, maxNumOfOutputs).map(movie => ({
                    title: movie.title ? movie.title : null,
                    rating: movie.vote_average ? movie.vote_average : null,
                    year: movie.release_date ? movie.release_date.substring(0, 4) : null
                }));

                // here setState is required in order to wait until the data is fetched from the API
                this.setState({
                    filteredMovies: filteredMovies
                });
            });
        }
    }

    onUserInputChange = e => {
        const userInput = e.currentTarget.value;
        this.setState({
            userInput: userInput,
            filteredMovies: []
        });
        
        // if possible, update and display the dropdown list of filtered movies
        this.fetchDataFromApi(userInput);
    }

    // necessary in order not to trigger the onBlur event
    // before a user selects a movie from the dropdown list
    onMouseDown = e => {
        e.preventDefault();
    }

    render() {
        const {
            onUserInputChange,
            onMouseDown,
            state: {
                userInput: userInput,
                filteredMovies: filteredMovies
            }
        } = this;

        let movieListComponent;

        if (filteredMovies.length) {
            movieListComponent = (
                <div>
                    <hr />
                    <div className="list-group dropdown-list">
                        {filteredMovies.map((movie, index) =>
                            <a
                                className="list-group-item list-group-item-action dropdown-list-item"
                                href="#"
                                key={index}
                                onMouseDown={(e) => onMouseDown(e)}
                                onClick={(e) => this.props.updateUserInputField(e, movie.title)}
                            >
                                <b>
                                    {movie.title ? movie.title : "Unknown Movie Title"}
                                </b>
                                <br />
                                <span className="text-small">
                                    {movie.rating ? movie.rating : "No "} Rating, {movie.year ? movie.year : "-"}
                                </span>
                            </a>
                        )}
                    </div>
                </div>
            );
        }

        return (
            <div className="container-fluid autocomplete-container">
                <input
                    type="text"
                    className="form-control shadow-none autocomplete-input"
                    value={userInput}
                    onChange={onUserInputChange}
                    onKeyDown={(e) => this.props.handleKeyDown(e)}
                    onBlur={(e) => this.props.closeAutocomplete(e)}
                    autoFocus
                />
                <textarea
                    className="form-control autocomplete-input-placeholder"
                    rows="2"
                    placeholder="&#013;Enter a movie name"
                    disabled
                />
                {movieListComponent}
            </div>
        );
    } 
}

export default Autocomplete;
