import React, { Component } from 'react';
import Autocomplete from './Autocomplete';
import movie_icon from "./icons/movie.svg";
import search_icon from "./icons/search.svg";
import './styles/search.sass';
import './styles/common.sass';


class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userInput: "",
            showAutocomplete: false
        };
    }

    openAutocomplete = e => {
        this.setState({
            userInput: e.currentTarget.value,
            showAutocomplete: true
        });
    }

    closeAutocomplete = e => {
        this.setState({
            userInput: e.currentTarget.value,
            showAutocomplete: false
        });
    }

    handleKeyDown = e => {
        // hide the autocomplete component if Esc button has been pressed
        if (e.keyCode === 27) {
            this.closeAutocomplete(e);
        }
    }

    updateUserInputField = (e, movieTitle) => {
        // necessary in order not to add the "#" symbol to the Url
        e.preventDefault();

        this.setState({
            userInput: movieTitle,
            showAutocomplete: false
        });
    }

    render() {
        const {
            openAutocomplete,
            closeAutocomplete,
            handleKeyDown,
            updateUserInputField,
            state: {
                userInput: userInput,
                showAutocomplete: showAutocomplete
            }
        } = this;

        let autocompleteComponent;
        if (showAutocomplete) {
            autocompleteComponent = (
                <Autocomplete
                    userInput={userInput}
                    updateUserInputField={updateUserInputField}
                    closeAutocomplete={closeAutocomplete}
                    handleKeyDown={handleKeyDown}
                />
            );
        }

        return (
            <div className="search-bar-area">
                <div className="input-group container-fluid search-bar">
                    <div className="input-group-prepend search-bar-icon">
                        <img className="movie-icon" src={movie_icon} alt="Movie icon" />
                    </div>
                    <input
                        type="text"
                        placeholder="Enter movie name"
                        className="form-control shadow-none search-bar-input"
                        value={userInput}
                        onFocus={openAutocomplete}
                        onChange={openAutocomplete}
                    />
                    <div className="input-group-append">
                        <span>
                            &nbsp;
                        </span>
                        <button className="search-bar-button">
                            <img className="search-icon" src={search_icon} alt="Search icon" />
                        </button>
                    </div>
                    {autocompleteComponent}
                </div>
           </div>
        );
    } 
}

export default Search;
